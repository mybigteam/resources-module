<?php

namespace MyBigTeam\Resources\Utils;

use Illuminate\Support\Fluent;

class SortCriteria
{
    public $sortInput;

    public $sortFields;

    public function __construct($sortInput)
    {
        $this->sortInput = $sortInput;
    }

    public function getSortFields()
    {
        if(!is_null($this->sortFields)) {
            return $this->sortFields;
        }

        $this->sortFields = [];

        foreach(explode(',', $this->sortInput) as $field) {
            $field = trim($field);

            if(!$field) {
                continue;
            }
            $order = 'asc';

            if(strpos($field, '-') === 0) {
                $field = substr($field, 1);
                $order = 'desc';
            }

            $field = str_replace('-', '_', $field);
            $this->sortFields[] = [$field, $order];
        }

        return $this->sortFields;
    }
}