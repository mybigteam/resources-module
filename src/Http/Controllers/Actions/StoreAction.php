<?php

namespace MyBigTeam\Resources\Http\Controllers\Actions;

use Illuminate\Http\Request;

trait StoreAction
{
    /**
     * @return array
     */
    public function store(Request $request)
    {
        $attributes = $request->toAttributes();
        $modelClass = $this->getModelClass();
        $model = new $modelClass;

        $this->validateAttributes($request, $model->getRules());
        
        $model->fill($attributes);

        $this->getService()->save($model);

        return $this
            ->response($model, $this->getTransformer())
            ->setStatusCode(201);
    }
}