<?php

namespace MyBigTeam\Resources\Http\Controllers\Actions;

use Illuminate\Http\Request;

trait ShowAction
{
    /**
     * @return array
     */
    public function show(Request $request)
    {
        $model = $this->getModelFromRoute($request);

        return $this
            ->response($model, $this->getTransformer())
            ->setStatusCode(200);
    }
}