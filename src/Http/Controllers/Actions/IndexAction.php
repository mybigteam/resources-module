<?php

namespace MyBigTeam\Resources\Http\Controllers\Actions;

use Illuminate\Http\Request;
use MyBigTeam\Resources\Utils\Filter;
use MyBigTeam\Resources\Utils\SortCriteria;
use InvalidArgumentException;

trait IndexAction
{
    /**
     * @return array
     */
    public function index(Request $request)
    {
        $perPage = $request->input('page.size', data_get($this, 'perPage'));

        $filter = $this->getFilterFromRequest($request);
        $sortCriteria = $this->getSortCriteriaFromRequest($request);
        $paginator = null;

        if($perPage) {
            $paginator = $this
                ->getRepository()
                ->findByFilterWithPagination($filter, $sortCriteria, $perPage);

            $models = $paginator->getCollection();
        } else {
            $models = $this
                ->getRepository()
                ->findByFilter($filter, $sortCriteria);
        }

        $fractal = fractal()
            ->collection($models)
            ->transformWith($this->getTransformer())
            ->withResourceName($this->getTransformer()->getResourceType());

        if($paginator) {
            $fractal->addMeta([
                'total-pages' => ceil($paginator->total() / $paginator->perPage()),
                'total-items' => $paginator->total(),
                'current-page' => $paginator->currentPage()
            ]);
        }

        return $fractal->toArray();
    }

    /**
     * @return Filter
     */
    public function getFilterFromRequest(Request $request)
    {
        $filter = new Filter($request->input('filter') ?: []);

        return $filter;
    }

    public function getSortCriteriaFromRequest(Request $request)
    {
        return new SortCriteria($request->input('sort'));
    }
}