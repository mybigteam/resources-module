<?php

namespace MyBigTeam\Resources\Http\Controllers\Actions;

use Illuminate\Http\Request;

trait UpdateAction
{
    /**
     * @return array
     */
    public function update(Request $request)
    {
        $model = $this->getModelFromRoute($request);

        $this->validateAttributes($request, $model->getRules(), true);

        $model->fill($request->toAttributes());

        $this->getService()->save($model);

        return $this
            ->response($model, $this->getTransformer())
            ->setStatusCode(200);
    }
}