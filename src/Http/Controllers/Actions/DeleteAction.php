<?php

namespace MyBigTeam\Resources\Http\Controllers\Actions;

use Illuminate\Http\Request;

trait DeleteAction
{
    /**
     * @return array
     */
    public function destroy(Request $request)
    {
        $model = $this->getModelFromRoute($request);

        $this->getService()->delete($model);

        return response()
            ->json(['meta' => []]);
    }
}