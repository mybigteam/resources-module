<?php

namespace MyBigTeam\Resources\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use MyBigTeam\Core\Http\Controllers\BaseController;
use MyBigTeam\Resources\Utils\Filter;
use MyBigTeam\Resources\Services\Service;
use MyBigTeam\Resources\Repositories\Repository;
use MyBigTeam\Resources\Transformers\Transformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use InvalidArgumentException;

/**
 * @method validateAttributes (see JsonApi\RequestValidator)
 */
abstract class ResourceController extends BaseController
{
    use Actions\IndexAction,
        Actions\StoreAction,
        Actions\ShowAction,
        Actions\UpdateAction,
        Actions\DeleteAction;

    /**
     * @return Service
     */
    abstract public function getService();

    /**
     * @return Repository
     */
    abstract public function getRepository();

    /**
     * @return Transformer
     */
    abstract public function getTransformer();

    public function getModelClass()
    {
        return $this
            ->getRepository()
            ->getModelClass();
    }

    public function getModelFromRoute(Request $request)
    {
        $modelId = $this->getModelIdFromRoute($request);
        $modelIdFromData = $request->input('data.id');

        if($modelIdFromData && $modelIdFromData != $modelId) {
            throw new ConflictHttpException(trans('resources::messages.resource_id_conflict'));
        }

        try {
            return $this->getRepository()->findOrFail($modelId);
        } catch(ModelNotFoundException $e) {
            abort(404, trans('resources::messages.resource_not_found'));
        }
    }

    /**
     * @return string
     */
    public function getModelIdFromRoute(Request $request)
    {
        $route = $request->route();

        $resourceTypeRequest = $this->getResourceTypeFromRequest($request);
        $resourceTypeRoute = $this->getResourceTypeRomRoute($route);

        $resourceType = $resourceTypeRequest ?: $resourceTypeRoute;

        if(!in_array($request->getMethod(), ['DELETE', 'GET']) && $resourceTypeRequest != $resourceTypeRoute) {
            throw new ConflictHttpException(trans('resources::messages.resource_type_conflict'));
        }

        return $route->parameter(str_singular($resourceType));
    }

    public function getResourceTypeFromRequest(Request $request)
    {
        return $request->input('data.type');
    }

    public function getResourceTypeRomRoute(Route $route)
    {
        $parameters = $route->parameterNames();
        return str_plural(end($parameters));
    }
}