<?php

namespace MyBigTeam\Resources\JsonApi;

use MyBigTeam\Core\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\Model;

class RequestValidator
{
    /**
     * @throws ValidationException
     */
    public function validateAttributes(
        BaseController $controller,
        Request $request,
        array $attributeRules,
        $withId = false
    )
    {
        $requestRules = $this->makeRequestRules($attributeRules, $withId);

        $controller->validate($request, $requestRules);
    }

    /**
     * @return array
     */
    public function makeRequestRules(array $attributeRules, $withId)
    {
        $rules = [
            'data' => 'array|required',
            'data.type' => 'required',
            'data.attributes' => 'array|required',
        ];

        if($withId) {
            $rules['data.id'] = 'required';
        }

        foreach($attributeRules as $name => $value) {
            $name = str_replace('_', '-', $name);
            $rules["data.attributes.{$name}"] = $value;
        }

        return $rules;
    }
}