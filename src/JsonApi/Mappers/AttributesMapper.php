<?php

namespace MyBigTeam\Resources\JsonApi\Mappers;

use Illuminate\Http\Request;

class AttributesMapper
{
    /**
     * @return array
     */
    public function toAttributes(Request $request)
    {
        $attributes = $request->input('data.attributes', []);

        return $this->parseAttributes($attributes);
    }

    /**
     * @return array
     */
    public function parseAttributes(array $attributes)
    {
        $realAttributes = [];

        foreach($attributes as $name => $value) {
            if(strpos($name, '-') !== false) {
                $name = str_replace('-', '_', $name);
            }
            $realAttributes[$name] = $value;
        }

        return $realAttributes;
    }
}