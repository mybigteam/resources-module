<?php

namespace MyBigTeam\Resources\Transformers;

class GenericTransformer extends Transformer
{
    private $modelClass;

    public function __construct($modelClass)
    {
        $this->modelClass = $modelClass;
    }

    public function getResourceType()
    {
        $shortName = with(new \ReflectionClass($this->modelClass))
            ->getShortName();

        return str_plural(kebab_case($shortName));
    }
}