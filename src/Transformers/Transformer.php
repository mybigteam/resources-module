<?php

namespace MyBigTeam\Resources\Transformers;

use MyBigTeam\Core\Transformers\BaseTransformer;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

abstract class Transformer extends BaseTransformer
{
    /**
     * @return array
     */
    public function transform(Model $resource)
    {
        $class = get_class($resource);

        return $this->transformAttributes(
            $this->getAllAttributes($resource)
        );
    }

    /**
     * Get resource attributes even if not filled
     */
    public function getAllAttributes(Model $resource)
    {
        $attributes = $resource->attributesToArray();

        foreach($resource->getFillable() as $key) {
            if(!array_key_exists($key, $attributes)) {
                $attributes[$key] = null;
            }
        }

        if(!array_key_exists('id', $attributes) && $resource->id) {
            $attributes['id'] = $resource->id;
        }

        return $attributes;
    }
}