<?php

namespace MyBigTeam\Resources\Services;

use MyBigTeam\Resources\Repositories\Repository;
use InvalidArgumentException;

class GenericService extends Service
{
    /**
     * @var string
     */
    public $repositoryClass;

    /**
     * @return array
     */
    public $repositoryInjection;

    public function __construct($repositoryClass, array $repositoryInjection = array())
    {
        $this->repositoryClass = $repositoryClass;
        $this->repositoryInjection = $repositoryInjection;
    }

    /**
     * @return string
     */
    protected function getRepositoryClass()
    {
        return $this->repositoryClass;
    }

    /**
     * @return array
     */
    public function getRepositoryInjection()
    {
        return $this->repositoryInjection;
    }
}