<?php

namespace MyBigTeam\Resources\Services;

use MyBigTeam\Resources\Repositories\Repository;
use InvalidArgumentException;
use Illuminate\Database\Eloquent\Model;

abstract class Service
{
    /**
     * @var Repository
     */
    protected $_repository;

    /**
     * @return Repository
     */
    public function getRepository()
    {
        return $this->_repository = $this->_repository
            ?: app($this->getRepositoryClass(), $this->getRepositoryInjection());
    }

    /**
     * @return string
     */
    protected function getRepositoryClass()
    {
        throw new InvalidArgumentException("Please configure the repository");
    }

    /**
     * @return array
     */
    public function getRepositoryInjection()
    {
        return [];
    }

    /**
     * @return bool
     */
    public function save(Model $model)
    {
        return $this->getRepository()->save($model);
    }

    /**
     * @return bool
     */
    public function delete(Model $model)
    {
        return $this->getRepository()->delete($model);
    }
}