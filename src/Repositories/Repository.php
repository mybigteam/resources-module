<?php

namespace MyBigTeam\Resources\Repositories;

use Illuminate\Database\Eloquent\Model;
use MyBigTeam\Resources\Utils\Filter;
use MyBigTeam\Resources\Utils\SortCriteria;
use InvalidArgumentException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

abstract class Repository
{
    /**
     * @var Model
     */
    private $_model;

    /**
     * @return Model
     */
    private function getModel()
    {
        return $this->_model = $this->_model
            ?: resolve($this->getModelClass());
    }
    
    /**
     * @return Model
     */
    public function getModelClass()
    {
        throw new InvalidArgumentException("Please configure the model");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findByFilter(Filter $filter, SortCriteria $sortCriteria)
    {
        $query = $this->query();

        $this->applyFilter($query, $filter);
        $this->applySortCriteria($query, $sortCriteria);

        return $query->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findByFilterWithPagination(Filter $filter, SortCriteria $sortCriteria, $perPage)
    {
        $query = $this->query();

        $this->applyFilter($query, $filter);
        $this->applySortCriteria($query, $sortCriteria);

        return $query->paginate($perPage);
    }

    public function applyFilter($query, Filter $filter) 
    {
        foreach($filter->getAttributes() as $attributeName => $attributeValue) {
            $attributeName = str_replace('-', '_', $attributeName);
            $this->applyFilterByAttribute($query, $attributeName, $attributeValue);
        }
    }

    public function applyFilterByAttribute($query, $attributeName, $attributeValue)
    {
        $scopeMethods = [
            camel_case($attributeName),
            'by'.ucfirst(camel_case($attributeName))
        ];

        $model = $this->getModel();
        
        foreach($scopeMethods as $scopeMethod) {
            $scopeInstanceMethod = 'scope'.ucfirst($scopeMethod);
            if(method_exists($model, $scopeInstanceMethod)) {
                $query->$scopeMethod($attributeValue);
                return;
            }
        }

        if(!$this->isVisibleAttribute($attributeName)) {
            throw new BadRequestHttpException("invalid filter '{$attributeName}'");
        }

        $query->where($attributeName, '=', $attributeValue);
    }

    public function applySortCriteria($query, SortCriteria $sortCriteria) 
    {
        foreach($sortCriteria->getSortFields() as $sortField) {
            $this->applySortCriteriaByAttribute($query, $sortField[0], $sortField[1]);
        }
    }

    public function applySortCriteriaByAttribute($query, $attributeName, $attributeOrder) 
    {
        if(!$this->isVisibleAttribute($attributeName)) {
            throw new BadRequestHttpException("invalid sort '{$attributeName}'");
        }

        $query->orderBy($attributeName, $attributeOrder);
    }

    public function isVisibleAttribute($attributeName)
    {
        $model = $this->getModel();

        $visibleFields = $model->getVisible() ?: $model->getFillable();

        return in_array($attributeName, $visibleFields);
    }

    /**
     * @return bool
     */
    public function save(Model $model)
    {
        return $model->save();
    }

    /**
     * @return Model
     */
    public function findById($id)
    {
        return $this->query()->find($id);
    }

    /**
     * @return Model
     */
    public function findOrFail($id)
    {
        return $this->query()->findOrFail($id);
    }

    /**
     * @return bool
     */
    public function delete(Model $model)
    {
        return $model->delete();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function query()
    {
        return $this
            ->getModel()
            ->newQuery()
            ->select($this->getModel()->getTable().'.*');
    }
}