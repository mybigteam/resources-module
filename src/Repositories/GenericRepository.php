<?php

namespace MyBigTeam\Resources\Repositories;

use Illuminate\Database\Eloquent\Model;

class GenericRepository extends Repository
{
    private $modelClass;

    public function __construct($modelClass)
    {
        $this->modelClass = $modelClass;
    }

    /**
     * @return Model
     */
    public function getModelClass()
    {
        return $this->modelClass;
    }
}