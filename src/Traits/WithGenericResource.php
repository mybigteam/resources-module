<?php

namespace MyBigTeam\Resources\Traits;

use MyBigTeam\Resources\Services\GenericService;
use MyBigTeam\Resources\Repositories\GenericRepository;
use MyBigTeam\Resources\Transformers\GenericTransformer;

trait WithGenericResource
{
    /**
     * @var GenericService
     */
    public $_service;

    /**
     * @var GenericRepository
     */
    public $_repository;

    /**
     * @var GenericTransformer
     */
    public $_transformer;

    public function getService()
    {
        $injected = [
            'repositoryClass' => GenericRepository::class,
            'repositoryInjection' => [
                'modelClass' => $this->getModelClass()
            ]
        ];

        return $this->_service = $this->_service
            ?: app(GenericService::class, $injected);
    }

    public function getRepository()
    {
        return $this->_repository = $this->_repository
            ?: $this->getService()->getRepository();
    }

    public function getTransformer()
    {
        $injected = [
            'modelClass' => $this->getModelClass()
        ];

        return $this->_transformer = $this->_transformer
            ?: app(GenericTransformer::class, $injected);
    }

    public function getModelClass()
    {
        throw new \InvalidArgumentException("Please define the method getModelClass");
    }
}