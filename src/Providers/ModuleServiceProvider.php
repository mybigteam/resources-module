<?php

namespace MyBigTeam\Resources\Providers;

use Illuminate\Http\Request;
use MyBigTeam\Core\Http\Controllers\BaseController;
use MyBigTeam\Core\Traits\GetModuleNameFromNamespace;
use MyBigTeam\Core\Providers\BaseModuleServiceProvider;
use MyBigTeam\Resources\JsonApi\Mappers\AttributesMapper;
use MyBigTeam\Resources\JsonApi\RequestValidator;
use Illuminate\Pagination\Paginator;

class ModuleServiceProvider extends BaseModuleServiceProvider
{
    use GetModuleNameFromNamespace;

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        $this->bootMappers();
    }

    public function register()
    {
        parent::register();

        Paginator::currentPageResolver(function () {
            $page = $this->app['request']->input('page.number');

            if (filter_var($page, FILTER_VALIDATE_INT) !== false && (int) $page >= 1) {
                return $page;
            }

            return 1;
        });
    }

    /**
     * @return string
     */
    public function getBaseDirectory()
    {
        return realpath(__DIR__.'/../..');
    }

    public function bootMappers()
    {
        $this->macroRequest(AttributesMapper::class, 'toAttributes');
        $this->macroController(RequestValidator::class, 'validateAttributes');
    }

    public function macroRequest($className, $method)
    {
        $this->macroClass(Request::class, $className, $method);
    }

    public function macroController($className, $method)
    {
        $this->macroClass(BaseController::class, $className, $method);
    }

    public function macroClass($macroClassName, $className, $method)
    {
        $macroClassName::macro($method, function() use($className, $method) {
            $target = app($className);
            $arguments = func_get_args();
            array_unshift($arguments, $this);
            return call_user_func_array([$target, $method], $arguments);
        });
    }
}