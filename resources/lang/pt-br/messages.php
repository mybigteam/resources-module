<?php

return [
    'resource_not_found' => 'Recurso não encontrado',
    'resource_type_conflict' => 'Conflito no tipo do recurso',
    'resource_id_conflict' => 'Conflito no id do recurso',
];