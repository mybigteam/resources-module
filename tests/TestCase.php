<?php

namespace MyBigTeam\Resources\Tests;

use MyBigTeam\Auth\Tests\AuthenticatedTestCase;

class TestCase extends AuthenticatedTestCase
{
    public function setUp()
    {
        parent::setUp();
    }

    /**
     * @return void
     */
    protected function getPackageProviders($app)
    {
        return array_merge(parent::getPackageProviders($app), [
            'MyBigTeam\Resources\Providers\ModuleServiceProvider',
            'MyBigTeam\Resources\Tests\App\CustomServiceProvider'
        ]);
    }
}