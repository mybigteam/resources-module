<?php

namespace MyBigTeam\Resources\Tests\App;

use MyBigTeam\Resources\Http\Controllers\ResourceController;
use MyBigTeam\Resources\Traits\WithGenericResource;

class BookController extends ResourceController
{
    use WithGenericResource;

    public function getModelClass()
    {
        return Book::class;
    }
}