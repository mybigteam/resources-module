<?php

use MyBigTeam\Resources\Tests\App\Book;

$factory->define(Book::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'release_date' => '2000-01-01',
        'pages' => $faker->numberBetween(50, 100),
    ];
});