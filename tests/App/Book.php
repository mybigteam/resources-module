<?php

namespace MyBigTeam\Resources\Tests\App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    const ATTRIBUTES = [ 'name', 'pages', 'release_date' ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = self::ATTRIBUTES;

    protected $visible = self::ATTRIBUTES;

    protected $casts = [
        'pages' => 'integer',
        'release_date' => 'date',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'deleted_at' => 'datetime',
    ];

    public function getRules()
    {
        return [
            'name' => 'required',
            'release_date' => 'date|required',
        ];
    }

    public function scopeReleasedAfter($query, $after)
    {
        return $query->where('release_date', '>=', $after);
    }
}
