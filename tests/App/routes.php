<?php

Route::group([
    'middleware' => ['api', 'auth:api'],
    'prefix' => config('core-module.route-prefix'),
    'namespace' => 'MyBigTeam\Resources\Tests\App',
], function () {
    Route::resource('books', 'BookController');
});