<?php

use MyBigTeam\Resources\Tests\TestCase;
use MyBigTeam\Resources\Tests\App\Book;

class ResourceTest extends TestCase
{
    public function testIndex()
    {
        $books = factory(Book::class, 3)->create();
        
        $this
            ->json('GET', '/api/v1/books')
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    [
                        'type' => 'books',
                        'attributes' => [
                            'release-date' => '2000-01-01 00:00:00'
                        ]
                    ],
                    [ 'type' => 'books' ],
                    [ 'type' => 'books' ],
                ],
            ]);
    }

    public function testPaginatedIndex()
    {
        $books = factory(Book::class, 10)
            ->create();
        
        $response = $this
            ->json('GET', '/api/v1/books', [
                'page' => ['number' => 1, 'size' => 2]
            ])
            ->assertStatus(200)
            ->assertJson([
                'meta' => [
                    'total-pages' => 5,
                    'total-items' => 10,
                ],
                'data' => [
                    [ 'id' => 1 ],
                    [ 'id' => 2 ],
                ],
            ]);

        $data = $response->json()['data'];
        $this->assertCount(2, $data);

        $this
            ->json('GET', '/api/v1/books', [
                'page' => ['number' => 2, 'size' => 2]
            ])
            ->assertStatus(200)
            ->assertJson([
                'meta' => [
                    'total-pages' => 5,
                    'current-page' => 2,
                    'total-items' => 10,
                ],
                'data' => [
                    [ 'id' => 3 ],
                    [ 'id' => 4 ],
                ],
            ]);
    }

    public function testFilteredIndex()
    {
        factory(Book::class)->create([
            'name' => 'my book one',
            'release_date' => '2000-01-01',
        ]);

        factory(Book::class)->create([
            'name' => 'my book two',
            'release_date' => '2000-02-01',
        ]);

        $response = $this
            ->json('GET', '/api/v1/books', [
                'filter' => ['name' => 'my book one']
            ])
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    [
                        'attributes' => [
                            'name' => 'my book one'
                        ]
                    ],
                ],
            ]);

        $data = $response->json()['data'];

        $this->assertCount(1, $data);

        $response = $this
            ->json('GET', '/api/v1/books', [
                'filter' => ['released-after' => '2000-01-15']
            ])
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    [
                        'attributes' => [
                            'name' => 'my book two'
                        ]
                    ],
                ],
            ]);

        $data = $response->json()['data'];

        $this->assertCount(1, $data);
    }

    public function testSortedIndex()
    {
        factory(Book::class)->create([
            'name' => 'my book one',
            'release_date' => '2000-01-01',
        ]);

        factory(Book::class)->create([
            'name' => 'my book two',
            'release_date' => '2000-02-01',
        ]);

        $response = $this
            ->json('GET', '/api/v1/books', [
                'sort' => '-release-date,name'
            ])
            ->assertStatus(200);

        $data = $response->json()['data'];

        $this->assertCount(2, $data);

        $this->assertEquals('my book two', $data[0]['attributes']['name']);
        $this->assertEquals('my book one', $data[1]['attributes']['name']);

        $response = $this
            ->json('GET', '/api/v1/books', [
                'sort' => 'release-date'
            ])
            ->assertStatus(200);

        $data = $response->json()['data'];

        $this->assertCount(2, $data);

        $this->assertEquals('my book one', $data[0]['attributes']['name']);
        $this->assertEquals('my book two', $data[1]['attributes']['name']);

    }

    public function testCreate()
    {
        $request = [
            'data' => [
                'type' => 'books',
                'attributes' => [
                    'name' => 'The Big Book',
                    'pages' => 51,
                    'release-date' => '1986-11-11 00:00:00',
                ]
            ],
        ];
        
        $this
            ->json('POST', '/api/v1/books', $request)
            ->assertStatus(201)
            ->assertJson($request);
    }

    public function testCreateInvalid()
    {
        $this->throwHttpExceptions = false;
        $request = [
            'data' => [
                'type' => 'books',
                'attributes' => [
                    'name' => 'The Big Book',
                    'pages' => 51,
                    'release-date' => null,
                ]
            ],
        ];
        
        $this
            ->json('POST', '/api/v1/books', $request)
            ->assertStatus(422)
            ->assertJson([
                'errors' => [
                    [
                        'code' => 'validation',
                        'source' => [
                            'pointer' => '/data/attributes/release-date'
                        ]
                    ]
                ]
            ]);
    }

    public function testShow()
    {
        $book = factory(Book::class)->create();
        
        $this
            ->json('GET', '/api/v1/books/'.$book->id)
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'type' => 'books',
                    'attributes' => [
                        'release-date' => '2000-01-01 00:00:00'
                    ]
                ],
            ]);
    }

    public function testUpdate()
    {
        $book = factory(Book::class)->create([
            'name' => 'The Big Book',
            'pages' => 51,
            'release_date' => '1986-11-11',
        ]);

        $request = [
            'data' => [
                'type' => 'books',
                'id' => $book->id,
                'attributes' => [
                    'name' => 'The Big Book Changed',
                    'release-date' => '1986-11-11',
                ]
            ],
        ];
        
        $this
            ->json('PATCH', "/api/v1/books/{$book->id}", $request)
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'type' => 'books',
                    'id' => $book->id,
                    'attributes' => [
                        'name' => 'The Big Book Changed',
                        'pages' => 51,
                        'release-date' => '1986-11-11 00:00:00',
                    ]
                ],
            ]);

        $book = $book->fresh();

        $this->assertEquals('The Big Book Changed', $book->name);
    }

    public function testDelete()
    {
        $book = factory(Book::class)->create([
            'name' => 'The Big Book',
            'pages' => 51,
            'release_date' => '1986-11-11',
        ]);
        
        $this
            ->json('DELETE', "/api/v1/books/{$book->id}")
            ->assertStatus(200);
        
        $this->assertEmpty($book->fresh());
    }
}